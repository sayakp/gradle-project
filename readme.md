# Gradle Project 1

There are two folders:

- **utils:** Used to generate the `utils-1.3.5.jar` file that is used by `multi-project/core`.
- **multi-project:** This is a Gradle multi-project with the Api app and Utils library.

### Usage

- Depending on the OS the `homeDir` variable for the `build.gradle.kts` file in  `utils` and `core` should be changed to reflect the home directory location.
- **Utils:** From the utils directory run `./gradlew test` to execute the tests and `./gradlew jar` to generate the jar file. Or simply use `gradle build`.
- **multi-project:** From the multi-project directory run `./gradlew run`, the console output should be the result of running `Utils.isAllPositiveNumbers("12", "79")`

### Made using:

- Gradle 8.1.1
