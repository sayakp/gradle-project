package utils;

public class StringUtils {

    /**
     *  Checks it a number is positive or not.
     *  Currently limited to integer ranges.
     *  + or - symbols are not accepted and it will return false if they are in the input
     * @param str
     * @return
     */
    public static boolean isPositiveNumber(String str) {
        try {
            return org.apache.commons.lang3.StringUtils.isNumeric(str) && Integer.parseInt(str) > 0;
        } catch (NumberFormatException e){
            return false;
        }
    }
}