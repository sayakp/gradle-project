package utils;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {
    @Test
    void testIsPositiveNumber() {
        assertTrue(StringUtils.isPositiveNumber("1"));
        assertTrue(StringUtils.isPositiveNumber("123"));
        assertTrue(StringUtils.isPositiveNumber("2147483647"));
        assertFalse(StringUtils.isPositiveNumber("2147483648"));
        assertFalse(StringUtils.isPositiveNumber("99999999999999999"));
        assertFalse(StringUtils.isPositiveNumber("0"));
        assertFalse(StringUtils.isPositiveNumber("-1123"));
        assertFalse(StringUtils.isPositiveNumber("aaaaaaaa"));
        assertFalse(StringUtils.isPositiveNumber("1a2d3f"));
    }
}
