val homeDir = System.getenv("HOME")

plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    implementation(project(":core"))
    implementation(files("$homeDir/libs/utils-1.3.5.jar"))
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(8))
    }
}

application {
    // Define the main class for the application.
    mainClass.set("api.App")
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "api.App"
    }
}
