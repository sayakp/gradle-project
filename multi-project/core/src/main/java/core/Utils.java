package core;
import utils.StringUtils;

public class Utils {
    public static boolean isAllPositiveNumbers(String... str){
        if(str.length==0)return false;

        for (String value:str
             ) {
             if(!StringUtils.isPositiveNumber(value))return false;
        }

        return true;
    }
}
